provider "aws" {
  region = "ap-southeast-1"
  profile = "admin-production"
}

# variable "vpc_subnet_info" {
#   description = "vpc subnet info"
#   type = list(object({
#     name = string,
#     cidr = string
#   }))
# }

# variable "my_ip" {}
# variable "public_key" {}
# variable "nginx_install" {}

# resource "aws_vpc" "my-app" {
#   cidr_block       = var.vpc_subnet_info[0].cidr
#   instance_tenancy = "default"
#   enable_dns_hostnames = true

#   tags = {
#     Name = "${var.vpc_subnet_info[0].name}-vpc"
#   }
# }

# resource "aws_subnet" "my-app-subnet" {
#   vpc_id     = aws_vpc.my-app.id
#   cidr_block = var.vpc_subnet_info[1].cidr
#   availability_zone = "ap-southeast-1a"
#   map_public_ip_on_launch = true

#   tags = {
#     Name = "${var.vpc_subnet_info[1].name}-subnet"
#   }
# }

# resource "aws_internet_gateway" "my-app-igw" {
#   vpc_id = aws_vpc.my-app.id

#   tags = {
#     Name = "${var.vpc_subnet_info[0].name}-igw"
#   }
# }

# resource "aws_route_table" "my-app-rtb" {
#   vpc_id = aws_vpc.my-app.id

#   route {
#     cidr_block = "0.0.0.0/0"
#     gateway_id = aws_internet_gateway.my-app-igw.id
#   }

#   tags = {
#     Name = "${var.vpc_subnet_info[0].name}-rtb"
#   }
# }

# resource "aws_route_table_association" "my-app-rtb-subnet" {
#   subnet_id      = aws_subnet.my-app-subnet.id
#   route_table_id = aws_route_table.my-app-rtb.id
# }

# resource "aws_security_group" "my-app-sg" {
#   name        = "my-app-sg"
#   vpc_id      = aws_vpc.my-app.id

#   ingress {
#     description      = "allow ssh"
#     from_port        = 22
#     to_port          = 22
#     protocol         = "tcp"
#     cidr_blocks      = [var.my_ip]
#   }

#   ingress {
#     description      = "allow nginx"
#     from_port        = 8080
#     to_port          = 8080
#     protocol         = "tcp"
#     cidr_blocks      = ["0.0.0.0/0"]
#   }

#   egress {
#     from_port        = 0
#     to_port          = 0
#     protocol         = "-1"
#     cidr_blocks      = ["0.0.0.0/0"]
#   }

#   tags = {
#     Name = "my-app-sg"
#   }
# }

# data "aws_ami" "latest-amazon-linux2" {
#   most_recent      = true
#   owners           = ["amazon"]

#   filter {
#     name   = "name"
#     values = ["amzn2-ami-kernel-*-x86_64-gp2"]
#   }
# }

# resource "aws_key_pair" "my-app-keypair" {
#   key_name   = "my-app-keypair"
#   public_key = file(var.public_key)
# }

# resource "aws_instance" "my-app-server" {
#   ami           = data.aws_ami.latest-amazon-linux2.id
#   instance_type = "t2.micro"
#   subnet_id = aws_subnet.my-app-subnet.id
#   vpc_security_group_ids = [aws_security_group.my-app-sg.id]
#   key_name = aws_key_pair.my-app-keypair.key_name

#   user_data = file(var.nginx_install)

#   tags = {
#     Name = "My App Server"
#   }
# }