resource "aws_vpc" "devops-gitlab" {
  cidr_block       = var.vpc_subnet_info[0].cidr
  instance_tenancy = "default"
  enable_dns_hostnames = true

  tags = {
    Name = "${var.vpc_subnet_info[0].name}-vpc"
  }
}

resource "aws_internet_gateway" "devops-gitlab-igw" {
  vpc_id = aws_vpc.devops-gitlab.id

  tags = {
    Name = "${var.vpc_subnet_info[0].name}-igw"
  }
}

output "vpc_id" {
  value = aws_vpc.devops-gitlab.id
}

output "vpc_igw" {
  value = aws_internet_gateway.devops-gitlab-igw
}