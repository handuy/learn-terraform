resource "aws_subnet" "devops-gitlab-1b-private" {
  vpc_id     = var.vpc_id
  cidr_block = var.vpc_subnet_info[2].cidr
  availability_zone = var.vpc_subnet_info[2].az
  map_public_ip_on_launch = false

  tags = {
    Name = var.vpc_subnet_info[2].name
  }
}

resource "aws_subnet" "devops-gitlab-1d-private" {
  vpc_id     = var.vpc_id
  cidr_block = var.vpc_subnet_info[4].cidr
  availability_zone = var.vpc_subnet_info[4].az
  map_public_ip_on_launch = false

  tags = {
    Name = var.vpc_subnet_info[4].name
  }
}

resource "aws_route_table" "devops-gitlab-private-rtb-1b" {
  vpc_id = var.vpc_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = var.devops-gitlab-natgw-1a-id
  }

  tags = {
    Name = "${var.vpc_subnet_info[0].name}-private-rtb-1b"
  }
}

resource "aws_route_table" "devops-gitlab-private-rtb-1d" {
  vpc_id = var.vpc_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = var.devops-gitlab-natgw-1c-id
  }

  tags = {
    Name = "${var.vpc_subnet_info[0].name}-private-rtb-1d"
  }
}

resource "aws_route_table_association" "devops-1b-rtb-subnet" {
  subnet_id      = aws_subnet.devops-gitlab-1b-private.id
  route_table_id = aws_route_table.devops-gitlab-private-rtb-1b.id
}

resource "aws_route_table_association" "devops-1d-rtb-subnet" {
  subnet_id      = aws_subnet.devops-gitlab-1d-private.id
  route_table_id = aws_route_table.devops-gitlab-private-rtb-1d.id
}