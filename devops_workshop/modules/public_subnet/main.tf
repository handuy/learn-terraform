resource "aws_subnet" "devops-gitlab-1a-public" {
  vpc_id     = var.vpc_id
  cidr_block = var.vpc_subnet_info[1].cidr
  availability_zone = var.vpc_subnet_info[1].az
  map_public_ip_on_launch = true

  tags = {
    Name = var.vpc_subnet_info[1].name
  }
}

resource "aws_subnet" "devops-gitlab-1c-public" {
  vpc_id     = var.vpc_id
  cidr_block = var.vpc_subnet_info[3].cidr
  availability_zone = var.vpc_subnet_info[3].az
  map_public_ip_on_launch = true

  tags = {
    Name = var.vpc_subnet_info[3].name
  }
}

resource "aws_route_table" "devops-gitlab-public-rtb" {
  vpc_id = var.vpc_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = var.vpc_igw.id
  }

  tags = {
    Name = "${var.vpc_subnet_info[0].name}-public-rtb"
  }
}

resource "aws_route_table_association" "devops-gitlab-1a-rtb" {
  subnet_id      = aws_subnet.devops-gitlab-1a-public.id
  route_table_id = aws_route_table.devops-gitlab-public-rtb.id
}

resource "aws_route_table_association" "devops-gitlab-1c-rtb" {
  subnet_id      = aws_subnet.devops-gitlab-1c-public.id
  route_table_id = aws_route_table.devops-gitlab-public-rtb.id
}

resource "aws_eip" "devops-gitlab-eip-1a" {
  vpc      = true
  depends_on = [
    var.vpc_igw
  ]
}

resource "aws_eip" "devops-gitlab-eip-1c" {
  vpc      = true
  depends_on = [
    var.vpc_igw
  ]
}

resource "aws_nat_gateway" "devops-gitlab-natgw-1a" {
  allocation_id = aws_eip.devops-gitlab-eip-1a.id
  subnet_id     = aws_subnet.devops-gitlab-1a-public.id

  tags = {
    Name = "devops-gitlab-natgw-1a"
  }

  depends_on = [var.vpc_igw]
}

resource "aws_nat_gateway" "devops-gitlab-natgw-1c" {
  allocation_id = aws_eip.devops-gitlab-eip-1c.id
  subnet_id     = aws_subnet.devops-gitlab-1c-public.id

  tags = {
    Name = "devops-gitlab-natgw-1c"
  }

  depends_on = [var.vpc_igw]
}

output "nat-gw-1a-id" {
  value = aws_nat_gateway.devops-gitlab-natgw-1a.id
}

output "nat-gw-1c-id" {
  value = aws_nat_gateway.devops-gitlab-natgw-1c.id
}