provider "aws" {
  region = "ap-southeast-1"
  profile = "admin-development"
}

# module "devops-gitlab-vpc" {
#   source = "./modules/vpc"
#   vpc_subnet_info = var.vpc_subnet_info_root  
# }

# module "devops-gitlab-public-subnet" {
#   source = "./modules/public_subnet"
#   vpc_id = module.devops-gitlab-vpc.vpc_id
#   vpc_subnet_info = var.vpc_subnet_info_root
#   vpc_igw = module.devops-gitlab-vpc.vpc_igw
# }

# module "devops-gitlab-private-subnet" {
#   source = "./modules/private_subnet"
#   vpc_id = module.devops-gitlab-vpc.vpc_id
#   vpc_subnet_info = var.vpc_subnet_info_root
#   devops-gitlab-natgw-1a-id = module.devops-gitlab-public-subnet.nat-gw-1a-id
#   devops-gitlab-natgw-1c-id = module.devops-gitlab-public-subnet.nat-gw-1c-id
# }